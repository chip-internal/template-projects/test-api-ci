import { expect } from 'chai';
import models from '../../src/models/index';

describe('User', function() {
  it('can create a user', (done) => {
    models.User.create({ name: 'Test User' }).then((newUser) => {
      expect(newUser.name).to.eq('Test User');
      done();
    }).catch(done);
  });
});
