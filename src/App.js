import Blipp from 'blipp';

export default class App {
  constructor(server) {
    this._server = server;
  }

  async configure() {
    await this._server.register(Blipp);
    this._loadControllers();
  }

  _loadControllers() {
  }

  async start() {
    await this.configure();
    this._server.start();
  }
}
